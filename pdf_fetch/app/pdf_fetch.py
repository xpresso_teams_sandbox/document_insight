import os
import sys
import cv2
import types
import pickle
import shutil
import random
import marshal
import pytesseract
from PIL import Image
from tqdm import tqdm
from pathlib import Path
from pdf2image import convert_from_path
from google.colab.patches import cv2_imshow
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = pdf_fetch
## $xpr_param_component_type = pipeline_job
# drive.mount('/content/drive')
# uploaded = files.upload()
def create_folders(outpath):
  if not os.path.exists(outpath):
    os.makedirs(outpath)
  else:
    shutil.rmtree(outpath)
    os.makedirs(outpath)
  return True
def pdf_to_image(pdf_file,outpath=None,page_index=None,start=None,end=None,dpi=None):
  # Store all the pages of the PDF in a variable
  try:
    print('pdf file: ', str(pdf_file))
    file_name = Path(pdf_file).stem
    extract_loc = outpath+'/'+file_name
    create_and_flush = create_folders(outpath=extract_loc)
    pages = convert_from_path(pdf_file,dpi=dpi,last_page=end) 
    if start is not None:
      start = start
    else:
      start =0
    if end is None:
      end = len(pages)
    else:
      end = end+1

    if page_index is not None:
      start,end = page_index, page_index+1
    print('Start - End ', str(start),str(end))
    for page in pages[start:end]: 
        filename = extract_loc+'/'+file_name+"_page_"+str(start)+".jpg"
        page.save(filename, 'JPEG')  
        start+=1
    return True
  except Exception as e:
    print(e,str(pdf_file)) 
def run_pdf_fetch(inpath=None,outpath=None,page_index=None,start=None,end=None,dpi=300):
  list_of_files = os.listdir(inpath)
  pdf_files = [f for f in list_of_files if f.endswith('.pdf')]
  for file in tqdm(pdf_files):
      extracted = pdf_to_image(pdf_file=inpath+'/'+file,outpath=outpath,page_index=page_index,start=start,end=end,dpi=dpi)
  return True
inpath = 'sample'
outpath = 'extracted'
flag = create_folders(outpath=outpath)
run_pdf_fetch(inpath=inpath,end=20,outpath=outpath)
