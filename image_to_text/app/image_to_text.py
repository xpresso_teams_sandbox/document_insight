import os
import sys
import cv2
import types
import pickle
import shutil
import random
import marshal
import pytesseract
from PIL import Image
from tqdm import tqdm
from pathlib import Path
from pdf2image import convert_from_path
from google.colab.patches import cv2_imshow
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = image_to_text
## $xpr_param_component_type = pipeline_job
inpath_ocr = 'preprocessed'
outpath_ocr = 'ocr_output'
create_ocr_folders = create_folders(outpath=outpath_ocr)
if os.path.exists(inpath_ocr):
  list_of_folds = os.listdir(inpath_ocr)
  for pdf_fold in tqdm(list_of_folds):
    pdf_img_loc = inpath_ocr+'/'+pdf_fold +'/'
    output_folder = outpath_ocr+'/'+pdf_fold
    create = create_folders(outpath=output_folder)
    pdf_imgs = os.listdir(pdf_img_loc)
    pdf_imgs = [i for i in pdf_imgs if i.endswith('.jpg')]
    for img in pdf_imgs:
      try:
        txt_file = Path(img).stem + '.txt'
        f = open(output_folder+'/'+txt_file, "w") 
        text = str((pytesseract.image_to_string(pdf_img_loc+img)))
        text = text.replace('-\n', '')       
        f.write(text)
        f.close()
      except Exception as e:
        print(e,str(img))

# f = open(outfile, "w") 
  
# for i in range(1, filelimit + 1): 
#     filename = "page_"+str(i)+".jpg"    
#     # text = str(((pytesseract.image_to_string(Image.open(filename))))) 
#     img = cv2.imread(filename)

#     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     gray = cv2.GaussianBlur(gray, (5, 5), 0)

#     #ret, imgf = cv2.threshold(gray, 0, 255,cv2.THRESH_BINARY,cv2.THRESH_OTSU)
#     imgf = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,31,2)
#     cv2_imshow(imgf)
#     text = str((pytesseract.image_to_string(imgf)))
#     # h, w, c = img.shape
#     # boxes = pytesseract.image_to_boxes(imgf) 
#     # for b in boxes.splitlines():
#     #     b = b.split(' ')
#     #     img = cv2.rectangle(imgf, (int(b[1]), h - int(b[2])), (int(b[3]), h - int(b[4])), (122, 255, 0), 5)
#     #     cv2_imshow(imgf)
#     text = text.replace('-\n', '')       
#     f.write(text) 
# f.close() 

# img = cv2.imread("delete-pages-in-pdf.jfif")

# h, w, c = img.shape
# boxes = pytesseract.image_to_boxes(img) 
# for b in boxes.splitlines():
#     b = b.split(' ')
#     img = cv2.rectangle(img, (int(b[1]), h - int(b[2])), (int(b[3]), h - int(b[4])), (122, 255, 0), 1)

# cv2_imshow(img)
# cv2.waitKey(0)

# d = pytesseract.image_to_data(img, output_type=Output.DICT)
# print(d.keys())
# n_boxes = len(d['text'])
# for i in range(n_boxes):
#     if int(d['conf'][i]) > 60:
#         (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
#         img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 1)

# cv2_imshow(img)
