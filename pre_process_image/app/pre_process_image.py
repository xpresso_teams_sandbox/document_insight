import os
import sys
import cv2
import types
import pickle
import shutil
import random
import marshal
import pytesseract
from PIL import Image
from tqdm import tqdm
from pathlib import Path
from pdf2image import convert_from_path
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = pre_process_image
## $xpr_param_component_type = pipeline_job
inpath_preprocess = 'extracted'
outpath_preprocess = 'preprocessed'
def preprocess(filename,outpath=None,grayscale=True,blur=True,blur_kernel_window=5,adaptiveThreshold=True):
  try:
    out_file_name = Path(filename).stem
    img = cv2.imread(filename)
    if grayscale:
      img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    if blur:
      img = cv2.GaussianBlur(img, (blur_kernel_window, blur_kernel_window), 0)
    if adaptiveThreshold:
      img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,31,2)
    cv2.imwrite(outpath+'/'+out_file_name+'.jpg',img)
    return True
  except Exception as e:
    print(e, str(filename))
    return False
create_preprocess_folders = create_folders(outpath=outpath_preprocess)
if os.path.exists(inpath_preprocess):
  list_of_folds = os.listdir(inpath_preprocess)
  for pdf_fold in tqdm(list_of_folds):
    pdf_img_loc = inpath_preprocess+'/'+pdf_fold +'/'
    output_folder = outpath_preprocess+'/'+pdf_fold
    create = create_folders(outpath=output_folder)
    pdf_imgs = os.listdir(pdf_img_loc)
    pdf_imgs = [i for i in pdf_imgs if i.endswith('.jpg')]
    for img in pdf_imgs:
      preprocess(filename=pdf_img_loc+img,outpath=output_folder)
