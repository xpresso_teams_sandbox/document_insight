pdf2image==1.14.0
pytesseract==0.3.6
tqdm==4.51.0
opencv_python==4.4.0.46
Pillow==8.0.1
protobuf==3.14.0
