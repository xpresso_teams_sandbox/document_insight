pdf2image==1.14.0
opencv_python==4.4.0.46
pytesseract==0.3.6
Pillow==8.0.1
tqdm
