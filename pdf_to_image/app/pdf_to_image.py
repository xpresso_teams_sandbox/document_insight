import os
import sys
import cv2
import types
import pickle
import shutil
import random
import marshal
import pytesseract
from PIL import Image
from pdf2image import convert_from_path
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    file_name = pickle.load(open(f"{PICKLE_PATH}/file_name.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = pdf_to_image
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["file_name"]

def set_image_dpi(file_path):
    im = Image.open(file_path)
    length_x, width_y = im.size
    factor = min(1, float(1024.0 / length_x))
    size = int(factor * length_x), int(factor * width_y)
    im_resized = im.resize(size, Image.ANTIALIAS)
    im_resized.save(file_path, dpi=(300, 300))
    return True

# Path of the pdf 
file_name = "/data/sample_pdf_1"

pdf_file = file_name+".pdf"

# Store all the pages of the PDF in a variable 
pages = convert_from_path(pdf_file, 500) 
image_counter = 1
  
for page in pages[:5]: 
    filename = "page_"+str(image_counter)+".jpg"
    page.save(filename, 'JPEG')
    set_image_dpi(filename)
    image_counter += 1

    

try:
    pickle.dump(file_name, open(f"{PICKLE_PATH}/file_name.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

